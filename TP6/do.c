#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "makeargv.h"

#define MAX_PID 10

void usage() {
	printf("do [--and|--or] command...\n");
	exit(EXIT_FAILURE);
}

int exec_cmd(char **cmd) {
	pid_t pid;
	
	switch (pid = fork()) {
	case -1:
		perror("fork");
		exit(EXIT_FAILURE);
	case 0:
		execvp(cmd[0], cmd);
		fprintf(stderr, "command not found : %s\n", cmd[0]);
		exit(EXIT_FAILURE);
	default:
		break;
	}

	return pid;
}

int main(int argc, char *argv[]) {
	static int or_flag;
	static int and_flag;
	static int cc_flag;
	static int kill_flag;
	int c, i, ncmd = 0, status, retvalue;
	char **cmdargv;
	pid_t list_pid[MAX_PID];

	/* Setting up getopt */
	while(1) {
		static struct option long_options[] = {
			{  "or", no_argument,   &or_flag, 1},
			{ "and", no_argument,  &and_flag, 1},
			{  "cc", no_argument,   &cc_flag, 1},
			{"kill", no_argument, &kill_flag, 1},
			{0, 0, 0, 0}
		};
		
		c = getopt_long(argc, argv, "", long_options, NULL);

		if (c == -1)
			break;
	}

	/* Executing all the commands */
	for (; argv[optind] != NULL; optind++) {
		cmdargv = makeargv(argv[optind]);
		list_pid[ncmd] = exec_cmd(cmdargv);
		ncmd++;
	}

	/* We need commands */
	if (ncmd == 0) {
		fprintf(stderr, "Missing commands\n");
 		usage();
	}

	if (cc_flag) {
		/* We're only waiting one command to finish */
		wait(&status);
		retvalue = WEXITSTATUS(status);
		
		if (kill_flag) {
			/* Then we're killing all other */
			for (i = 0; i < ncmd; i++) {
				kill(list_pid[i], SIGTERM);
			}
		}
	} else {
		/* Waiting that all commands executed are finished and create return value */
		for (i = 0; i < ncmd; i++) {
			wait(&status);

			/* or_flag use &= and and_flag use |= because retvalue == 0 means true */
			if (i == 0)
				retvalue  = WEXITSTATUS(status);
			else if (or_flag)
				retvalue &= WEXITSTATUS(status);
			else
				retvalue |= WEXITSTATUS(status);
			
		}
	}
	freeargv(cmdargv);
	return retvalue;
}
