#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv) {
	int fd;
	int bufsize = atoi(getenv("MCAT_BUFSIZ"));
	char *buf;

	buf = malloc(bufsize);

	if (buf == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	if (argc != 2) {
		exit(EXIT_FAILURE);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd == -1) {
		perror("open");
		return EXIT_FAILURE;
	}

	while (read(fd, buf, bufsize) > 0) {
		write(STDOUT_FILENO, buf, bufsize);
	}

	return EXIT_SUCCESS;
}
