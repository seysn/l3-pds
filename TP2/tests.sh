#!/bin/bash

# Initializing tests folder and files
mkdir tests

> tests/foo
> /tmp/foobar
for i in {1..100}
do
	echo "aaaaaaaa" >> tests/foo
	echo "aaaaaaaa" >> /tmp/foobar
done
ln -s /tmp/foobar tests/

# Apparent size
echo "Test 1 : Apparent size"
echo $ du -b tests
du -b tests
echo $ ./mdu -b tests
./mdu -b tests
echo

# Real size
echo "Test 2 : Real size"
echo $ du -B 512 test
du -B 512 tests
echo $ ./mdu tests
./mdu tests
echo

# Follow links + Apparent size
echo "Test 3 : Follow links + Apparent size"
echo $ du -L -b tests
du -L -b tests
echo $ ./mdu -L -b tests
./mdu -L -b tests
echo

# Follow links + Real size
echo "Test 4 : Follow links + Real size"
echo $ du -L -B 512 tests
du -L -B 512 tests
echo $ ./mdu -L tests
./mdu -L tests

# The end
rm -rf tests
