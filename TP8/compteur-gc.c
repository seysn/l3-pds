#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>
#include <ctype.h>

#define NB_THREADS 4

pthread_t thread_ids[NB_THREADS];

struct pt_compteur_s {
	char *bloc;
	unsigned long taille;
};

unsigned long compteur_gc(char *bloc, unsigned long taille) {
    unsigned long cptr = 0;
	unsigned long i;

    for (i = 0; i < taille; i++) {
        if (bloc[i] == 'G' || bloc[i] == 'C') {
            cptr++;
		}
	}
	
    return cptr;
}

void *pt_compteur_gc(void *arg) {
	struct pt_compteur_s *args = (struct pt_compteur_s *) arg;
	return (void *) compteur_gc(args->bloc, args->taille);
}

int main(int argc, char *argv[]) {
    struct stat st;
    int fd, lus, i;
    char *tampon;
    unsigned long cptr = 0;
	void *value;
    off_t taille = 0;
    struct timespec debut, fin;
	struct pt_compteur_s * s_comp;
	int nb_threads;
	pthread_t *threads;
	
    assert(argv[1] != NULL);
	
    /* Quelle taille ? */
    assert(stat(argv[1], &st) != -1);
    tampon = malloc(st.st_size);
    assert(tampon != NULL);

    /* Chargement en mémoire */ 
    fd = open(argv[1], O_RDONLY);
    assert(fd != -1);
    while ((lus = read(fd, tampon + taille, st.st_size - taille)) > 0)
        taille += lus;
    assert(lus != -1);
    assert(taille == st.st_size);
    close(fd);

	if (argc == 3) {
		if (isdigit(argv[2][1])) {
			nb_threads = (int) strtol(argv[2], (char **) NULL, 10);
		} else {
			fprintf(stderr, "invalide number of threads\n");
			exit(EXIT_FAILURE);
		}
	} else {
		nb_threads = NB_THREADS;
	}
	threads = malloc(sizeof(pthread_t *) * (nb_threads - 1));
	
	assert(clock_gettime(CLOCK_MONOTONIC, &debut) != -1);
	
	/* Création des threads */
    for (i = 1; i < nb_threads; i++) {
		s_comp = malloc(sizeof(struct pt_compteur_s));
		s_comp->taille = taille / (nb_threads - 1);
		s_comp->bloc = tampon + (i - 1) * s_comp->taille;
        pthread_create(&threads[i - 1], NULL, &pt_compteur_gc, s_comp);
    }
	
    /* Calcul proprement dit */
    /* cptr = compteur_gc(tampon, taille); */
	for (i = 1; i < nb_threads; i++) {
		pthread_join(threads[i - 1], &value);
		cptr += (long) value;
    }
	
	
    assert(clock_gettime(CLOCK_MONOTONIC, &fin) != -1);
	free(s_comp);
	
    /* Affichage des résultats */
    printf("Nombres de GC:   %ld\n", cptr);
    printf("Taux de GC:      %lf\n", ((double) cptr) / ((double) taille));

    fin.tv_sec  -= debut.tv_sec;
    fin.tv_nsec -= debut.tv_nsec;
    if (fin.tv_nsec < 0) {
        fin.tv_sec--;
        fin.tv_nsec += 1000000000;
    }
    printf("Durée de calcul: %ld.%09ld\n", fin.tv_sec, fin.tv_nsec);
    printf("(Attention: très peu de chiffres après la virgule sont réellement significatifs !)\n");

    return 0;
}
