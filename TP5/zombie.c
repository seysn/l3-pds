#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

typedef void (*func_t) (void *);

void machin(void *args) {
	printf("SALUT J'AI EU ÇA : %s\n", (char *) args);
	printf("MAINTENANT JE VAIS DORMIR\n");
	sleep(1);
	printf("OHLALA J'AI BIEN DORMI\n");
}

void forkfork(func_t f, void *arg) {
	pid_t pid1, pid2;
	int status;
	
	printf("Je suis le père %d\n", getpid());
	fflush(stdout);
	switch(pid1 = fork()) {
	case -1:
		perror("fork");
		exit(EXIT_FAILURE);
	case 0:
		printf("Je suis le fils %d\n", getpid());
		fflush(stdout);
		switch(pid2 = fork()) {
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		case 0:
			printf("Je suis le petit-fils %d\n", getpid());
			fflush(stdout);
			f(arg);
			printf("Je suis le petit-fils, et mon père après mon dodo est %d\n", getppid());
			exit(EXIT_SUCCESS);
		default:
			exit(pid2);
			break;
		}
	default:
		wait(&status);
		if (WIFEXITED(status))
			printf("Je suis le père, et j'ai reçu ça : %d\n", WTERMSIG(status));
		break;
	}
}

int main(void) {
	forkfork(machin, "OUI");
	return 0;
}
