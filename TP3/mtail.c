#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <libgen.h>

#define BUFSIZE 4096

void usage() {
	printf("Usage:\n");
	printf("  mtail [options] <path>\n");
	printf("\n");
	printf("Options:\n");
	printf("  -n NUM\tOutput the last NUM lines.\n");

	exit(EXIT_FAILURE);
}

int nb_lines_buf(char *buf, size_t size) {
	int res = 0, i;

	for (i = 0; i < size; i++) {
		if (buf[i] == '\n')
			res++;
	}
		
	return res;
}

int nb_lines_file(int fd) {
	char buf[BUFSIZE];
	int res = 0, ret_bytes;
	
	if (lseek(fd, 0, SEEK_SET) == -1) {
		perror("lseek");
		exit(EXIT_FAILURE);
	}
		
	while ((ret_bytes = read(fd, &buf, BUFSIZE)) > 0) {
		res += nb_lines_buf(buf, ret_bytes);
	}

	return res;
}

void simple_tail(int fd, int nlines) {
	int size_file = nb_lines_file(fd);
	int skip_lines = size_file - nlines;
	char buf[BUFSIZE];
	int ret_bytes, i;
		
	if (lseek(fd, 0, SEEK_SET) == -1) {
		perror("lseek");
		exit(EXIT_FAILURE);
	}

	while ((ret_bytes = read(fd, &buf, BUFSIZE)) > 0) {
		if (skip_lines <= 0) {
			write(STDOUT_FILENO, buf, ret_bytes);
		} else {
			for (i = 0; i < ret_bytes && skip_lines > 0; i++) {
				if (buf[i] == '\n') {
					skip_lines--;
				}

				if (skip_lines <= 0) {
					write(STDOUT_FILENO, buf + i + 1, ret_bytes - i - 1);
				}
			}
		}
	}
}

/* Doesn't work for ntail bytes > BUFSIZE */
void effective_tail(int fd, int ntail, int bufsize) {
	char buf[BUFSIZE];
	int ret_bytes, ret_offset, nlines = 0, nbytes = 0, i;

	if ((ret_offset = lseek(fd, 0, SEEK_END)) == -1) {
		perror("lseek");
		exit(EXIT_FAILURE);
	}

	if (ret_offset <= BUFSIZE) {
		if (lseek(fd, 0, SEEK_SET) == -1) {
			perror("lseek");
			exit(EXIT_FAILURE);
		}

		while ((ret_bytes = read(fd, &buf, BUFSIZE)) > 0) {
			nlines = nb_lines_buf(buf, ret_bytes);
			nbytes += ret_bytes;
		}

		if (lseek(fd, -nbytes, SEEK_END) == -1) {
			perror("lseek");
			exit(EXIT_FAILURE);
		}

		while ((ret_bytes = read(fd, &buf, nbytes)) > 0) {
			write(STDOUT_FILENO, buf, nbytes);
		}
	} else {
		if ((ret_offset = lseek(fd, -bufsize, SEEK_END)) == -1) {
			perror("lseek");
			exit(EXIT_FAILURE);
		}
		
		while ((ret_bytes = read(fd, &buf, BUFSIZE)) > 0) {
			if (nlines > ntail) {
				write(STDOUT_FILENO, buf + i + 1, ret_bytes - i - 1);
			}
			for (i = ret_bytes; i >= 0; i--) {
				if (buf[i] == '\n') {
					nlines += 1;
					if (nlines > ntail) {
						write(STDOUT_FILENO, buf + i + 1, ret_bytes - i - 1);
						break;
					}
				}
			}
		}

		if (nlines < ntail) {
			effective_tail(fd, ntail, bufsize + BUFSIZE);
		}
	}
}

int main(int argc, char **argv) {
	int fd, nlines = 10, opt;
	char *filename = NULL;

	while ((opt = getopt(argc, argv, "n:")) != -1) {
		switch (opt) {
		case 'n':
			nlines = atoi(optarg);
			break;
		case '?':
		default:
			usage();
		}
	}

	filename = argv[optind];
	if (filename == NULL) {
		printf("%s: Missing argument\n", basename(argv[0]));
		usage();
	}

	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror("open");
		return EXIT_FAILURE;
	}
	
	effective_tail(fd, nlines, BUFSIZE);
	close(fd);
	return EXIT_SUCCESS;
}
