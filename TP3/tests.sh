#!/bin/bash

# Initializing tests folder and files
mkdir tests

> tests/foo

for i in {300..0}
do
	echo "This is the line number $i of this text file." >> tests/foo
done

> tests/bar

for i in {10..0}
do
	echo "This is the line number $i of this text file." >> tests/bar
done

# Test 1 : 10 last lines
echo "Test 1 : 10 last lines"
echo "$ tail tests/foo"
tail tests/foo
echo "$ ./mtail tests/foo"
./mtail tests/foo
echo -e "\n"

# Test 2 : lines in files > lines asked
echo "Test 2 : lines in files > lines asked"
echo "$ tail -n 20 tests/bar"
tail -n 20 tests/bar
echo "$ ./mtail tests/bar"
./mtail -n 20 tests/bar
echo -e "\n"

# Test 3 : newline in the end of file
echo >> tests/foo
echo "Test 3 : newline in the end of file"
echo "$ tail tests/foo"
tail tests/foo
echo "$ ./mtail tests/foo"
./mtail tests/foo

# The end
rm -rf tests
